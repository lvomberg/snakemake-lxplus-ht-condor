#!/usr/bin/bash
# properties = {properties}
# 
# condor-jobscript.sh
#
# Wrapper for snakemake jobs
#
# Copyright (C) 2019 Matthew Stone <mrstone3@wisc.edu>
# Distributed under terms of the MIT license.
. /etc/profile
tar xf /eos/user/l/lvomberg/venvs/snakemake.tar.gz
source snakemake/bin/activate
{exec_job}
