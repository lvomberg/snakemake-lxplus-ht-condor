#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Copyright © 2019 Matthew Stone <mrstone3@wisc.edu>
# Distributed under terms of the MIT license.

"""

"""

import argparse
import subprocess
import getpass
import re

def is_running(jobid):
    """
    Check if job ID is in list of currently running jobs from condor_q
    """

    # TODO make username configurable
    username = getpass.getuser()
    command = f"condor_q -constraint 'ClusterId=={jobid} ' -l | awk '/JobStatus/ {{print $3}}'"
    result = subprocess.run(command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, text=True)

    # this checks if the condor_q command was successful, NOT if the job is running or not. Eiher way, something is wrong so better to fail
    if result.returncode != 0:
        return False
    
    # Check the output for the job status
    output = result.stdout
    if output:
        match = re.search(r"(\d+)", output)
        job_status = int(match.group(1))
    else:
        return False
    if job_status < 3:#0: Unexpanded 1:Idle 2:Running 3:Removed 4:Completed 5:Held 6:Submission_err
        return jobid # return jobid instead of boolean, because snakemake docs say this function should do that
    else:
        return False
    
    



def parse_condor_history(res):
    """
    Scrape runtime states from condor_history -l

    - for now, just check exit status
    - later versions of condor support json output but not 8.4
    """
    
    info = dict()
    stdout = res.stdout.split(b'\n')
    for line in stdout:
        if line.startswith(b'ExitStatus'):
            info['ExitStatus'] = int(line.split()[-1])

    return info


def check_exit_status(jobid):
    """
    Check if job exited successfully
    """

    cmd = ["condor_history", "-l", jobid]
    res = subprocess.run(cmd, check=True, stdout=subprocess.PIPE)
    # with open('/cephfs/user/s6luvomb/code/snakemake-trial/man_logs', 'w') as infile:
    #         infile.write(json.dumps(cmd))
    info = parse_condor_history(res)

    
    return info['ExitStatus']


def condor_status(jobid):
    if is_running(jobid):
        print("running")
    elif check_exit_status(jobid) == 0:
        print("success")
    else:
        print("failed")


def main():
    parser = argparse.ArgumentParser(
        description=__doc__,
        formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument('jobid')
    args = parser.parse_args()

    condor_status(args.jobid)


if __name__ == '__main__':
    main()
